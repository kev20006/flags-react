import React from 'react';

import { useTheme } from '../../Contexts/ThemeContext';
import styled, { withTheme, StyledComponent } from 'styled-components';
import { cardColor } from '../../Theme/theme'

import './Navbar.scss';

interface Theme {
    mode: string;
}

interface NavProps {
    theme: Theme
}

const NavBarWrapper:StyledComponent<'nav', any, {}, never> = styled.nav`
  background-color: ${cardColor};
  box-shadow: 0 2px 4px rgba(0,0,0, 0.2);
  `;

const NavBar:any = ({ theme }:NavProps) => {
  const themeToggle = useTheme();

  return (
    <NavBarWrapper>
      <div className="NavWrapper">
        <h1>Where in the World?</h1>
        <div
          role="button"
          className="ToggleMode"
          onClick={()=>{themeToggle.toggle()}}>
            {theme.mode === 'dark'
              ? "Light Mode"
              : "Dark Mode"
            }
        </div>
      </div>
    </NavBarWrapper>
  )
};

export default withTheme(NavBar);