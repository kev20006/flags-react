import React, { useContext } from 'react';

import { DropDown } from '../../../UI/DropDown/DropDown';
import { FlagContext } from '../../../Contexts/FlagContext';

const Filter = () => {
  const { filter } = useContext(FlagContext);
  return (
    <div>
      <DropDown
        placeholder="Filter by Region"
        contents={["All Regions", "Africa", "Americas", "Asia", "Europe", "Oceania"]}
        callback={(term)=>{filter(term)}}
      />
  </div>
  )
  
};

export default Filter;