import React from 'react';

import Search from './Search/Search';
import Filter from './Filter/Filter';

import './SearchAndFilter.scss';

const SearchAndFilter = () => (
    <header className="SearchAndFilter">
        <Search />
        <Filter />
    </header>
);

export default SearchAndFilter;