import React, { useState, useEffect, useContext } from 'react';
import styled, { StyledComponent } from 'styled-components';

import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Card, { ICard } from '../../../UI/Card/Card'
import Input from '../../../UI/Input/Input';
import { FlagContext } from '../../../Contexts/FlagContext';


export const SearchWrapper: StyledComponent<({ children, width, className }: ICard) => JSX.Element, any, {}, never> = styled(Card)`
  display: flex;
  align-items: center;
  padding: 0 2rem;
  font-size: 1rem;
`;

const Search = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const { search } = useContext(FlagContext);

  const [debouncedSearchTerm, emptyString] = useDebounce(searchTerm, 500);

  useEffect(()=>{
      if (debouncedSearchTerm || emptyString) {
        search(debouncedSearchTerm);
      }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearchTerm])

  return (
    <SearchWrapper width="80%">
      <FontAwesomeIcon  icon={faSearch} />
      <Input
        placeholder="Select A Country"
        value={searchTerm}
        keyPressed={setSearchTerm}></Input>
    </SearchWrapper>
  );
};

const useDebounce = (value: string, delay:number): [string, boolean] => {
  const [debounceValue, setDebounceValue] = useState<string>(value);

  useEffect(() => {
    const handler = setTimeout(()=>{
      setDebounceValue(value)
    }, delay);

    return () => {
        clearTimeout(handler);
    };
  },[value, delay]);

  return [debounceValue, !debounceValue? true : false];
}

export default Search;