import React, { useContext } from 'react';

import FlagCard from './FlagCard/FlagCard';

import { FlagContext } from '../../Contexts/FlagContext'

import './FlagDisplay.scss';

const FlagDisplay = () => {
  const { flags } = useContext(FlagContext)

  return (
    <main className='FlagWrapper'>
      {flags.map((flag: any)=>(
          <FlagCard
            key = {flag.name}
            flagPath = {flag.flag}
            countryName= {flag.name}
            population = {flag.population}
            region= {flag.subregion}
            capital= {flag.capital}
        />
      ))}
    </main>
)};

export default FlagDisplay;



