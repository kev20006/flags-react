import React from 'react';
import styled, { StyledComponent } from 'styled-components';

import Card, { ICard } from '../../../UI/Card/Card'
import List from '../../../UI/List/List';
import { StyledLink } from '../../../UI/StyledComponents/StyledComponents';


export const StyledFlagCard: StyledComponent<({ children, width, className }: ICard) => JSX.Element, any, {}, never> = styled(Card)`
  margin-bottom: 2rem;
  padding-bottom: 2rem;
  max-width: 80%;
  img {
    width: 100%;
  }
  text-decoration: none;
  color: inherit;
  `;

interface FlagProps {
  flagPath: string,
  countryName: string,
  population: number,
  region: string,
  capital: string
}

const FlagCard = (
  {
    flagPath,
    countryName,
    population,
    region,
    capital
  }: FlagProps) => {

  return (
    <StyledLink to={countryName} className="FlagCard">
      <StyledFlagCard>
        <img src={flagPath} alt={`${countryName} Flag`}/>
        <h4>
          {countryName}
        </h4>
        <List
          titles={["Country Name: ", "Population: ", "Region: ", "Capital: "]}
          values={[countryName, population, region, capital]}
        />
      </StyledFlagCard>
    </StyledLink>
  );
}

export default FlagCard;
