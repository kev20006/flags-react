import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { IFlagContext, FlagContext } from '../../Contexts/FlagContext';
import { StyledLink, SmallContentWrapper } from '../../UI/StyledComponents/StyledComponents';
import List from '../../UI/List/List';
import BordersList from './BordersList/BordersList';
import styled, { StyledComponent } from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';


const CountryWrapper: StyledComponent<'main', any, {}, never> = styled.div`
  display: flex;
  flex-direction: column;
  padding:0 1rem;
  `;

const Country = () => {
  const { country } = useParams<{country: string}>();
  const { allFlags, flags } = useContext<IFlagContext>(FlagContext);
  const countryDetails = flags.find(flag => (
    flag.name.toLowerCase() === country.toLowerCase()
  ));

  return (
      <CountryWrapper>
      <span style={{margin: "2rem 0"}}>
        <SmallContentWrapper width="4rem">
          <StyledLink to="/">
            <FontAwesomeIcon  icon={faArrowLeft} />
            <span style={{marginLeft: "1rem"}}>Back</span>
          </StyledLink>
        </SmallContentWrapper>
      </span>
      <img
        src={countryDetails?.flag}
        alt={`${countryDetails?.name} flag`}
        style={{height: "100%"}}
      />
      <h3> {countryDetails?.name} </h3>
      <List
        titles={["Native Name: ", "Population: ", "Region: ", "Capital: "]}
        values={[countryDetails?.nativeName, countryDetails?.population, countryDetails?.region, countryDetails?.subregion, countryDetails?.capital]}
      />
      <List
        titles={["Top Level Domain: ", "Currencies: ", "Languages: "]}
        values={[
          countryDetails?.topLevelDomain,
          countryDetails?.currencies.map(currency => currency.name),
          countryDetails?.languages.map(language => language.name)
        ]}
      />
      <BordersList
        borders={countryDetails?.borders}
        allFlags={allFlags}
      />
    </CountryWrapper>
  )
}

export default Country;