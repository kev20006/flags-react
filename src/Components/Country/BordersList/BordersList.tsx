import React from 'react';
import { StyledLink, SmallContentWrapper } from '../../../UI/StyledComponents/StyledComponents';
import styled, { StyledComponent } from 'styled-components';
import { IFlag } from '../../../Interfaces/Interfaces';

interface IBorders {
  borders: string[] | undefined
  allFlags: IFlag[] | undefined
}

const BordersWrapper: StyledComponent<'div', any, {}, never> = styled.div`
  display: flex;
  flex-wrap: wrap;
  `;

const getNameFromCode =( code: string, flagArr: IFlag[]|undefined ): IFlag | null| undefined => {
  return flagArr
  ? flagArr.find( element => element.alpha3Code === code)
  : null;
}

const BordersList = ({ borders, allFlags }:IBorders) => (
  <>
    <h4>
      Border Countries
    </h4>
    <BordersWrapper>
      {
        borders
          ? borders.map((border) => (
            <StyledLink key={border} to={`/${getNameFromCode(border, allFlags)?.name}`}>
              <SmallContentWrapper key={border}>
                <span>{getNameFromCode(border, allFlags)?.name}</span>
              </SmallContentWrapper>
            </StyledLink>
        ))
        : null
      }
    </BordersWrapper>
  </>
);

export default BordersList