import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import FlagDisplay from './Components/FlagDisplay/FlagDisplay';
import NavBar from './Components/Navbar/Navbar';
import SearchAndFilter from './Components/SearchAndFilter/SearchAndFilter'
import Country from './Components/Country/Country'

import FlagProvider from './Contexts/FlagContext';

import './App.scss';

const App:React.FC =  () => {
  return (
    <div className='AppWrapper'>
      <Router>
        <NavBar />
        <FlagProvider>
          <Switch>
            <Route path='/:country'>
              <Country />
            </Route>
            <Route path='/'>
              <SearchAndFilter />
              <FlagDisplay />
            </Route>
          </Switch>
        </FlagProvider>
      </Router>
    </div>
)};

export default App;
