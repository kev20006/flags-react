import React, { useState, useContext, createContext } from "react";
import styled, { ThemeProvider, StyledComponent } from 'styled-components';
import { backgroundColor, textColor } from '../Theme/theme';
import { IThemeState } from '../Interfaces/Interfaces'

const ThemeToggleContext = createContext({
  toggle: () => {
    // placeholder toggle function passed in with the MyThemeProvider component below
  }
});

export const useTheme= () => useContext(ThemeToggleContext);

interface ThemeProps {
  children: React.ReactNode
}

const Wrapper:StyledComponent<'div', any, {}, never> = styled.div`
  background-color: ${backgroundColor};
  color: ${textColor};
  `;

export const MyThemeProvider = ({ children }: ThemeProps) => {
  const [themeState, setThemeState] = useState<IThemeState>({
    mode: 'light'
  });

  const toggle = ():void => {
      const mode: string = themeState.mode === 'light'
        ? 'dark'
        : 'light';
      setThemeState({ mode })
  };

  return (
      <ThemeToggleContext.Provider
        value= {{ toggle }}
      >
        <ThemeProvider
          theme={{ mode: themeState.mode }}
        >
          <Wrapper>
          {children}
          </Wrapper>
        </ThemeProvider>
      </ThemeToggleContext.Provider>
  )
}

export default ThemeProvider;