import React, { useState, createContext, useEffect } from "react";
import { IFlag } from '../Interfaces/Interfaces'

type sAndFFunction = ( term: string ) => void

export interface IFlagContext {
  allFlags: IFlag[]
  flags: IFlag[]
  filter: sAndFFunction
  search: sAndFFunction
}

interface FlagProps {
  children: React.ReactNode
}

export const FlagContext = createContext<IFlagContext>({
  allFlags: [],
  flags: [],
  filter: ( term: string ) => {
      // placeholder, function definition provided in FlagsProvider
  },
  search: ( term: string ) => {
    // placeholder, function definition provided in FlagsProvider
  }
});

export const FlagsProvider = ({ children }: FlagProps) => {
  const [flags, setFlags] = useState<IFlag[]>([]);
  const [currentFlags, setCurrentFlags] = useState<IFlag[]>([]);
  const [ currentFilter, setCurrentFilter ] = useState<string>('All Regions');
  const [ currentSearch, setCurrentSearch ] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(true);

  // Get all flags from API on init
  useEffect(()=>{
    if (loading) {
      fetch("https://restcountries.eu/rest/v2/all")
        .then((res)=> res.json())
        .then((res:IFlag[])=>{
          setFlags(res);
          setCurrentFlags(res);
          setLoading(true);
      });
    }
  },[loading]);

  // Listen to search and filter terms, if they either changes refilter the flags
  useEffect(()=>{
    setCurrentFlags(
      flags
        .filter((flag: IFlag)=> {
          if (currentFilter === 'All Regions') {
            return flag
          }
          else {
            return flag.region === currentFilter ? flag : null
          }
        })
        .filter((flag: IFlag)=> {
          if (currentSearch === '') {
            return flag
          }
          else {
            return flag.name.toLowerCase().includes(currentSearch.toLowerCase())
              ? flag
              : null
          }
        })
      );
  },[currentSearch, currentFilter, flags])

  return (
      <FlagContext.Provider
        value= {{ 
          allFlags: flags,
          flags:currentFlags,
          search: setCurrentSearch,
          filter: setCurrentFilter }}
      >
          {children}
      </FlagContext.Provider>
  )
}

export default FlagsProvider;