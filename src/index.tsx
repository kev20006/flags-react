import React from 'react';
import ReactDOM from 'react-dom';

import { MyThemeProvider } from './Contexts/ThemeContext';

import './index.css';

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <MyThemeProvider>
      <App />
    </MyThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
