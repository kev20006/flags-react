import theme from 'styled-theming';

export const backgroundColor:theme.ThemeSet  = theme('mode', {
  light: 'hsl(0, 0%, 98%)',
  dark: 'hsl(207, 26%, 17%)'
});

export const textColor:theme.ThemeSet = theme('mode', {
  light: 'hsl(200, 15%, 8%)',
  dark: 'hsl(0, 0%, 100%)'
});

export const cardColor:theme.ThemeSet  = theme('mode', {
  light: 'hsl(0, 0%, 100%)',
  dark: 'hsl(209, 23%, 22%)'
});

export const inputText: theme.ThemeSet = theme('mode', {
  light: 'hsl(0, 0%, 52%)',
  dark: 'hsl(0, 0%, 100%)'
});