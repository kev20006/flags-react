export interface IThemeState {
    mode: string
}

export interface IFlag {
    name: string
    flag: string
    population: number
    subregion: string
    region: string
    capital: string
    nativeName: string
    topLevelDomain: string[]
    currencies: ICurrency[]
    languages: ILanguage[]
    borders: string[]
    alpha3Code: string
}

interface ICurrency {
    code: string
    name: string
    symbol: string
}

interface ILanguage {
    iso639_1: string
    iso639_2: string
    name: string
    nativeName: string
}

export interface SandFOptions {
    SFValue: string | null;
    SFMode: string | null;
  }