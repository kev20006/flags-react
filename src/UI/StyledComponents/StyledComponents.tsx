import styled, { StyledComponent } from "styled-components";
import { Link } from "react-router-dom";
import Card, { ICard } from "../Card/Card";

export const StyledLink  = styled(Link)`
  text-decoration: none;
  color: inherit;
  `;

export const SmallContentWrapper: StyledComponent<({ children, width, className }: ICard) => JSX.Element, any, {}, never> = styled(Card)`
  font-size: 14px;
  margin: 0.5rem 0;
  margin-right: 0.5rem;
  padding: 0.5rem 1rem;
  width: 3rem;
  text-decoration: none;
  color: inherit;
  `;