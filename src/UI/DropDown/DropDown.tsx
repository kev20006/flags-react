import React, { useState } from 'react';
import styled, { StyledComponent } from 'styled-components';

import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { backgroundColor } from '../../Theme/theme';

import Card from '../Card/Card';

import './DropDown.scss';

type ICallback  = (filter: string) => void


interface IDropDownProps {
    placeholder: string
    contents: string[]
    callback: ICallback
}

const ListItem: StyledComponent<'li', any, {}, never> = styled.li`
  padding: 0.5rem 0;

  &.Selected {
    background-color: ${backgroundColor};
  }

  &:hover {
    background-color: ${backgroundColor};
  }
  `;

export const DropDown = ({ placeholder, contents, callback }: IDropDownProps) => {
  const [dropDownOpen, setDropDownOpen] = useState<boolean>(false);
  const [selectedItem, setSelectedItem] = useState<string | null>(null);

  const selectListItem = (value: string): void => {
    setSelectedItem(value);
    setDropDownOpen(false);
    callback(value);
  };

  return (
    <>
      <Card width="40%" className='Button'>
        <div
          role="button" className='Button' onClick= {() => {
            dropDownOpen
              ?setDropDownOpen(false)
              :setDropDownOpen(true)
        }}>
          <p>
            {!selectedItem ? placeholder : selectedItem}
          </p>
          <div
            className='Icon'
            style={{
            transform: dropDownOpen ? 'rotate(90deg)' : 'rotate(0)',
          }}>
            <FontAwesomeIcon  icon={faCaretRight} />
          </div>
        </div>
      </Card>
      <Card width="40%" className={['Menu', dropDownOpen && 'Open'].join(' ')}>
        <div>
          <ul>
          {contents.map((element:string) => (
            <ListItem
              key={element}
              className={element === selectedItem ? 'Selected' : ''}
              onClick={()=>{selectListItem(element)}}
            >
              {element}
            </ListItem>
          ))}
          </ul>
        </div>
      </Card>
    </>
  );
}
