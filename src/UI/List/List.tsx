import React from 'react';
import styled, { StyledComponent } from 'styled-components';

interface IListValues {
    [position: number]: string | string[] | number | undefined
}

interface IListProps {
  titles: string[]
  values: IListValues
}

const valuesToString = (value: string[] | string | number | undefined): string => {
    if (value) {
      return Array.isArray(value) ? value.join(', ') : `${value}`
    }
    return ''
}

const StyledList: StyledComponent<'ul', any, {}, never> = styled.ul`
  margin: 0.5rem 0;
  padding: 0;
  `;

const StyledLi: StyledComponent<'li', any, {}, never> = styled.li`
  list-style: none;
  padding:  0.3rem;
  margin: 0;
  `;

const List = ({titles, values}: IListProps):JSX.Element => (
  <StyledList>
    {titles.map((element, index)=>(
        <StyledLi key={"item-" + index}>
        <strong>
          {element}
        </strong>
        {valuesToString(values[index])}
      </StyledLi>
    ))}
  </StyledList>
)

export default List


