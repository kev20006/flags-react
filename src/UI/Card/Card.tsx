import React from 'react'
import styled, { StyledComponent } from 'styled-components';

import { cardColor } from '../../Theme/theme'

export interface ICard {
    children: JSX.Element | JSX.Element[]
    width?: string
    className?: string
}
const CardWrapper: StyledComponent<'div', any, {}, never> = styled.div`
  background-color: ${cardColor};
  box-shadow: 2px 2px 4px rgba(0,0,0, 0.2);
  `;

const Card = ({ children, width="auto", className="" }: ICard) => (  
  <CardWrapper className={className} style={{width}}>
    { children }
  </CardWrapper>
);

export default Card;