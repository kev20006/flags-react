import React from 'react';
import styled, { StyledComponent } from 'styled-components';

import { cardColor, inputText } from '../../Theme/theme'

type ICallback  = (value: string) => void

interface IInput {
  placeholder: string;
  value: string;
  keyPressed?: ICallback | null;
}

const StyledInput: StyledComponent<'input', any, {}, never> = styled.input`
  background-color: ${cardColor};
  color: ${inputText};
  padding: 1.5rem 0;
  margin-left: 1rem;
  width: 100%;
  border: none;
  font-size: 1rem;
`;

const Input = ({ placeholder, value, keyPressed = null }: IInput) => (
  <StyledInput
    placeholder="Search for a country..."
    value={ value }
    onChange={ e => keyPressed ? keyPressed(e.target.value) : null }
  />
);

export default Input;
